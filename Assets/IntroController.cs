﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class IntroController : MonoBehaviour
{
    private string nextScene;

    public void OnStartClick()
    {
        nextScene = "Level01";
        GetComponent<Animator>().Play("IntroFade");
    }

    public void OnArenaClick()
    {
        nextScene = "RandomArena";
        GetComponent<Animator>().Play("IntroFade");
    }

    private void Update()
    {
        if (Input.GetButtonDown("Cancel")) Application.Quit();
    }

    public void OnFadeDone()
    {
        SceneManager.LoadScene(nextScene);
    }
}
