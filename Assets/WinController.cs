﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class WinController : MonoBehaviour
{
    
    public void OnRestart()
    {
        SceneManager.LoadScene("Intro");
    }

    public void OnExit()
    {
        Application.Quit();
    }
}
