
using System;
using UnityEngine;

public class Util
{
    public delegate float Curve(float t);

    public static float Linear(float t)
    {
        return t;
    }
    
    public static float SmoothFlash(float currentValue, float startTime, float duration, Curve curve = null)
    {
        if (curve == null) curve = Linear;
        var time = Time.time;
        var desiredValue = 0f;
        if (time < startTime + duration / 2)
        {
            var t = (time - startTime) / duration * 2;
            desiredValue = curve(t);
        }
        else if (time < startTime + duration)
        {
            var t = 1-(time - (startTime + duration / 2)) / duration * 2;
            desiredValue = curve(t);
        }

        return Interpolate(currentValue, duration, desiredValue);
    }

    public static float Interpolate(float currentValue, float duration, float desiredValue)
    {
        float speed = 1 / duration * 2;
        float delta = Mathf.Sign(desiredValue - currentValue) * speed * Time.deltaTime;
        if (Mathf.Abs(delta) > Math.Abs(currentValue - desiredValue))
        {
            return desiredValue;
        }
        else
        {
            return currentValue + delta;
        }
    }


    public static float Smooth(float currentValue, float startTime, float duration, Curve curve = null)
    {
        if (curve == null) curve = Linear;
        var time = Time.time;
        var desiredValue = 1f;
        if (time >= startTime && time <= startTime + duration)
        {
            desiredValue = curve((time - startTime) / duration);
        }

        return Interpolate(currentValue, duration, desiredValue);

    }
}
