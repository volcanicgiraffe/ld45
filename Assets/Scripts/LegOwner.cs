using System;
using UnityEngine;


//todo: may be rename to BodyController
public class LegOwner : MonoBehaviour
{
    public delegate void LegEvent(LegScript leg);
    
    public event LegEvent OnLegHit;
    public event LegEvent OnLegLost;

    private bool _wantToGrab = false;

    public Side _side = Side.None;
    internal EffectApplier _applier;

    public Side side
    {
        get { return _side; }
        set { SetSide(value); }
    }

    private void SetSide(Side value)
    {
        if (_side != value)
        {
            _applier.DrawCore(value, _side);
        }
        _side = value;
    }

    private void Start()
    {
        if (side == Side.None)
        {
            GetComponent<EnemyScript>()?.Release();
        }

        this._applier = EffectApplier.GetInstance(GetComponent<SpriteRenderer>());
        _applier.DrawCore(side, Side.None);
    }

    public event Action OnOwnerDead;
    public event Action OnDisconnect;

    public void OnPartHitCall(LegScript leg)
    {
        OnLegHit?.Invoke(leg);
    }

    public void OnOwnerDeadCall()
    {
        OnDeador.Instance.OnDead1(this.transform);
        OnOwnerDead?.Invoke();
    }
    
    
    public void OnPartLostCall(LegScript leg)
    {
        OnLegLost?.Invoke(leg);
    }

    public void SetWantToGrab(bool set)
    {
        _wantToGrab = set;
    }
    public bool WantToGrab()
    {
        return _wantToGrab;
    }

    public enum Side
    {
        None, Player, Enemy
    }

    public void ReleaseLegs()
    {
        OnDisconnect?.Invoke();
    }

    public void Revive()
    {
        GetComponent<Hitable>()?.Revive();
    }
}
