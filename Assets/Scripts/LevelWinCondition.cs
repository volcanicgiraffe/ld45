using System.Collections;
using UnityEngine;
using UnityEngine.SceneManagement;

public class LevelWinCondition : MonoBehaviour
{
    public void OnBossDead()
    {
        //todo: maybe some effect?
        StartCoroutine(DoWin());
    }

    private IEnumerator DoWin()
    {
        yield return new WaitForSeconds(2);
        SceneManager.LoadScene("WinScreen");
    }
}
