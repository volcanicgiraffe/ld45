﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class GlobalSounder
{
    public static GlobalSounderMB gsmb;

    public static void PlaySoundLock()
    {
        gsmb.PlaySoundLock();
    }

    public static void PlaySoundUnlock()
    {
        gsmb.PlaySoundUnlock();
    }
    public static void PlaySoundPuff()
    {
        gsmb.PlaySoundPuff();
    }
}

public class GlobalSounderMB : MonoBehaviour
{
    public AudioSource sounder;

    public AudioClip[] soundLock;
    public AudioClip[] soundUnlock;
    public AudioClip[] soundPuff;

    private void Start()
    {
        GlobalSounder.gsmb = this;
    }

    public void PlaySoundLock()
    {
        PlayRandomSound(soundLock);
    }

    public void PlaySoundUnlock()
    {
        PlayRandomSound(soundUnlock);
    }

    public void PlaySoundPuff()
    {
        PlayRandomSound(soundPuff);
    }
    void PlayRandomSound(AudioClip[] list)
    {
        if (list == null || list.Length == 0 || sounder == null) return;
        var sound = Random.Range(0, list.Length);
        sounder.clip = list[sound];
        if (sounder.clip != null) sounder.Play();
    }
}
