﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyOnHeroClose : MonoBehaviour
{
    private void OnTriggerEnter2D(Collider2D other)
    {
        GetComponentInParent<Animator>().SetTrigger("On Player In Attack Area");
        GetComponentInParent<Animator>().ResetTrigger("On Player Out Attack Area");
    }

    private void OnTriggerExit2D(Collider2D other)
    {
        GetComponentInParent<Animator>().SetTrigger("On Player Out Attack Area");
        GetComponentInParent<Animator>().ResetTrigger("On Player In Attack Area");
    }

}
