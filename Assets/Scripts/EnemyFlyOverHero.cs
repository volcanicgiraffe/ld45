﻿using System.Collections;
using UnityEngine;

public class EnemyFlyOverHero : StateMachineBehaviour
{
    private Rigidbody2D _rigidbody2D;
    private GameObject _player;
    private Vector3 _targetPoint;
    private int rotForce = 1;
    public float attackForce = 15f;
    public float maxPullForce = 500f;
    public float DistanceCoefficient = 100f;
    override public void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        _player = HeroSoulController.Hero;
        rotForce = Random.value > 0.5f ? -1 : 1;
        _targetPoint = new Vector3(_player.transform.position.x, _player.transform.position.y + 1f, _player.transform.position.z);
        _rigidbody2D = animator.GetComponent<Rigidbody2D>();
    }

    // OnStateUpdate is called on each Update frame between OnStateEnter and OnStateExit callbacks
    override public void OnStateUpdate(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        var direction = _targetPoint - animator.transform.position;
        direction.Normalize();
        _rigidbody2D.AddForce(new Vector2(direction.x, direction.y) * attackForce, ForceMode2D.Impulse);
        _rigidbody2D.AddTorque(rotForce*350f);
    }

}
