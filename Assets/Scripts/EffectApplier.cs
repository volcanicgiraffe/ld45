using System;
using System.Collections;
using UnityEngine;


public class EffectApplier : MonoBehaviour
{
    private MaterialPropertyBlock _block;
    private SpriteRenderer _renderer;

    public bool IsBlinking;
    private bool _isBlinking;

    public float InkFilling = 0;
    private float _oldFilling = 0;

    public float Damaging = 0;
    private float _prevDamage = 0;

    public float Glowing = 0;
    private float _prevGlowing = 0;


    private bool _isLeg = false;
    
    private void Awake()
    {
        init(GetComponent<SpriteRenderer>());
    }

    private EffectApplier init(SpriteRenderer renderer)
    {
        if (_renderer == null)
        {
            _renderer = renderer;
            var name = _renderer.sprite.name;
            var mask = Resources.Load<Texture2D>("Masks/mask1_" + name);
            _block = new MaterialPropertyBlock();
            _renderer.GetPropertyBlock(_block);
            _block.SetTexture("_MaskTex", mask);
            _renderer.SetPropertyBlock(_block);
            
            //prevent blinking if unity used invalid default
            _block.SetFloat("_BlinkStartedAt", -1);
            _renderer.SetPropertyBlock(_block);
        }
        return this;
    }

    public static EffectApplier GetInstance(SpriteRenderer obj)
    {
        var applier = obj.GetComponent<EffectApplier>() ?? obj.gameObject.AddComponent<EffectApplier>();
        return applier.init(obj);
    }

    private void StartBlinking()
    {
        if (_isBlinking) return;
        _block.SetFloat("_BlinkStartedAt", Shader.GetGlobalVector("_Time")[1]);
        _renderer.SetPropertyBlock(_block);
        _isBlinking = true;
    }

    private void StopBlinking()
    {
        if (!_isBlinking) return;
        _block.SetFloat("_BlinkStartedAt", -1);
        _renderer.SetPropertyBlock(_block);
        _isBlinking = false;
    }

    private void SetInkFilling(float ink)
    {
        _block.SetFloat("_Ink", ink);
        _renderer.SetPropertyBlock(_block);
    }

    private void SetDamage(float damage)
    {
        _block.SetFloat("_DamageFactor", damage);
        _renderer.SetPropertyBlock(_block);
    }


    public void DrawLegOnEnemy()
    {
        InkFilling = 1;
        IsBlinking = false;
        _renderer.color = EffectManager.emb?.LegEnemyTint ?? Color.white;
    }

    public void DrawLegFree()
    {
        InkFilling = 0;
        IsBlinking = true;
        _renderer.color = EffectManager.emb?.LegFreeTint ?? Color.white;
    }

    public void DrawLegOnUs()
    {
        InkFilling = 0;
        IsBlinking = false;
        _renderer.color = EffectManager.emb?.LegUsTint ?? Color.white;
    }

    public void DrawLeg(LegOwner.Side side)
    {
        _isLeg = true;
        switch (side)
        {
            case LegOwner.Side.Enemy:
                DrawLegOnEnemy();
                break;
            case LegOwner.Side.None:
                DrawLegFree();
                break;
            case LegOwner.Side.Player:
                DrawLegOnUs();
                break;
        }
    }

    public void DrawCoreOnEnemy()
    {
        InkFilling = 1;
        IsBlinking = false;
        Glowing = 0;
    }

    public void DrawCoreFree()
    {
        IsBlinking = true;
        InkFilling = 0;
        Glowing = 0;
    }

    public void DrawCoreOnUs()
    {
        InkFilling = 0;
        IsBlinking = false;
        Glowing = 0;
    }

    
    public void Update()
    {
        if (_oldFilling != InkFilling)
        {
            SetInkFilling(InkFilling);
            _oldFilling = InkFilling;
        }

        var canBlink = (_isLeg && HeroSoulController.IsInsideBody) || (!_isLeg && !HeroSoulController.IsInsideBody);

        if (IsBlinking && canBlink) StartBlinking();
        else StopBlinking();

        if (_prevDamage != Damaging)
        {
            SetDamage(Damaging);
            _prevDamage = Damaging;
        }
        
        if (_prevGlowing != Glowing)
        {
            SetGlowing(Glowing);
            _prevGlowing = Glowing;
        }
    }

    private void SetGlowing(float glowing)
    {
        _block.SetFloat("_Glow", glowing);
        _renderer.SetPropertyBlock(_block);
    }

    public void DrawCore(LegOwner.Side side, LegOwner.Side prevSide)
    {
        _isLeg = false;
        switch (side)
        {
            case LegOwner.Side.Enemy:
                DrawCoreOnEnemy();
                break;
            case LegOwner.Side.Player:
                DrawCoreOnUs();
                break;
            case LegOwner.Side.None:
                //this effect is not actually visible, but it may lead to bugs
                //if (prevSide == LegOwner.Side.Enemy)
                //{
                //    gameObject.AddComponent<EnemySoulOut>();
                //}
                //else
                //{
                    DrawCoreFree();
                //}
                break;
        }
    }
}