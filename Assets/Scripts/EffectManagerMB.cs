﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class EffectManager
{
    public static EffectManagerMB emb;
    public static void CreateSpark(Vector2 position)
    {
        emb.CreateSpark(position);
    }
    public static void CreateBigSpark(Vector2 position)
    {
        emb.CreateSpark(position, true);
    }

    public static void Slowdown(float force = 0.2f, float duration = 1)
    {
        emb.Slowdown(force, duration);
    }
    public static void CamShake(float force = 0.1f, float duration = 0.2f)
    {
        emb.Slowdown(force, duration);
    }
}


public class EffectManagerMB : MonoBehaviour
{
    public GameObject sparkPrefab;
    public GameObject bigSparkPrefab;
    public bool slowInProgress;
    public bool camshakeInProgress;


    public Color LegUsTint = new Color(1, 1, 1, 1);
    public Color LegEnemyTint = new Color(0.6f, 0.6f, 0.6f, 1f);
    public Color LegFreeTint = new Color(0.8f, 1, 0.8f, 1);
    
    internal void CreateSpark(Vector2 position, bool big = false)
    {
        Instantiate(big ? bigSparkPrefab : sparkPrefab, position, UnityEngine.Random.rotation);
    }

    internal void Slowdown(float force, float duration)
    {
        if (slowInProgress) return;
        StartCoroutine(SlowdownWaiter(force, duration));
    }

    internal void CameraShake(float force, float duration)
    {
        if (camshakeInProgress) return;
        StartCoroutine(CamshakeWaiter(force, duration));
    }

    private IEnumerator CamshakeWaiter(float magnitude, float duration)
    {
        camshakeInProgress = true;
        Vector3 originalPosition = Camera.main.transform.position;
        float elapsed = 0f;

        while (elapsed < duration)
        {
            float x = UnityEngine.Random.Range(Camera.main.transform.position.x - magnitude, Camera.main.transform.position.x + magnitude);
            float y = UnityEngine.Random.Range(Camera.main.transform.position.y - magnitude, Camera.main.transform.position.y + magnitude);

            Camera.main.transform.position = new Vector3(x, y, Camera.main.transform.position.z);
            elapsed += Time.deltaTime;
            yield return 0;
        }
        Camera.main.transform.position = originalPosition;
        camshakeInProgress = false;
    }

    private IEnumerator SlowdownWaiter(float force = 0.2f, float duration = 1)
    {
        slowInProgress = true;
        var steps = (1 - force) / 10;
        for (int i = 0; i < 10; i++)
        {
            Time.timeScale -= steps;
            yield return new WaitForSecondsRealtime(0.02f);
        }
        Time.timeScale = force;
        Time.fixedDeltaTime = 0.02f * Time.timeScale;

        yield return new WaitForSecondsRealtime(duration-0.2f);
        for (int i = 0; i < 10; i++)
        {
            Time.timeScale += steps;
            yield return new WaitForSecondsRealtime(0.02f);
        }
        Time.timeScale = 1;
        Time.fixedDeltaTime = 0.02f;
        slowInProgress = false;
    }

    // Start is called before the first frame update
    void Start()
    {
        EffectManager.emb = this;
    }

    // Update is called once per frame
    void Update()
    {

    }
}
