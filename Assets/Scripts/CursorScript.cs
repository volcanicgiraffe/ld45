﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CursorScript : MonoBehaviour
{
    private Rigidbody2D _rigidbody2D;
    public Rigidbody2D Hero;
    
    // Start is called before the first frame update
    void Start()
    {
        _rigidbody2D = GetComponent<Rigidbody2D>();
    }

    // Update is called once per frame
    void Update()
    {
        var mouse = Input.mousePosition;

        var position = Camera.main.ScreenToWorldPoint(mouse);

        _rigidbody2D.MovePosition(position);
    }
}
