﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyRetreat : StateMachineBehaviour
{
    private Rigidbody2D _rigidbody2D;
    private EnemyScript _enemy;
    private GameObject _player;

    // OnStateEnter is called when a transition starts and the state machine starts to evaluate this state
    override public void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        _rigidbody2D = animator.GetComponent<Rigidbody2D>();
        animator.ResetTrigger("Just Attacked");
        _player = HeroSoulController.Hero;
        _enemy = animator.GetComponent<EnemyScript>();
    }

    // OnStateUpdate is called on each Update frame between OnStateEnter and OnStateExit callbacks
    override public void OnStateUpdate(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        if (_player == null || _enemy == null) return;
        var direction = Mathf.Sign(_player.transform.position.x - _rigidbody2D.position.x);
        _rigidbody2D.AddForce(_rigidbody2D.position + new Vector2(-direction * _enemy.RetreatSpeed, 0), ForceMode2D.Impulse);
        _rigidbody2D.transform.localScale = new Vector3(-direction, 1, 1);

    }
}
