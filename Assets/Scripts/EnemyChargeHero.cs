﻿using System.Collections;
using UnityEngine;

public class EnemyChargeHero : StateMachineBehaviour
{
    private Rigidbody2D _rigidbody2D;
    private Vector3 _targetDirection;
    private GameObject _player;
    public float ChargeForce = 250f;
    public float rotImpulse = 350f;
    override public void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        _player = HeroSoulController.Hero;
        rotImpulse = rotImpulse * Random.value > 0.5f ? -1 : 1;
        _rigidbody2D = animator.GetComponent<Rigidbody2D>();
        _targetDirection = (_player.transform.position - _rigidbody2D.gameObject.transform.position).normalized;
        _rigidbody2D.AddTorque(rotImpulse, ForceMode2D.Impulse);
    }

    // OnStateUpdate is called on each Update frame between OnStateEnter and OnStateExit callbacks
    override public void OnStateUpdate(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        _rigidbody2D.AddForce(new Vector2(_targetDirection.x, _targetDirection.y) * ChargeForce, ForceMode2D.Impulse);
        
    }

}
