using System;
using UnityEngine;


public class CameraRelativeSprite : MonoBehaviour
{
    public Transform objectToFollow;

    public float OffsetFactor = 1f;

    private float _x;
    private void Start()
    {
        if (objectToFollow == null) objectToFollow = Camera.main.transform;
        _x = transform.position.x;
    }

    public void Update()
    {
        transform.position = new Vector3(_x + objectToFollow.position.x * OffsetFactor, transform.position.y,
            transform.position.z);
    }
}
