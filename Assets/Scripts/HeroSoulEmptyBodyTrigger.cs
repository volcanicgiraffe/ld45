﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HeroSoulEmptyBodyTrigger : MonoBehaviour
{
    private HeroSoulController _soul;
    // Start is called before the first frame update
    void Start()
    {
        _soul = GetComponentInParent<HeroSoulController>();
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        _soul.OnBodyTrigger(other);
    }
}
