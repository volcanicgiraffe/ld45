using System;
using UnityEngine;
using UnityEngine.Serialization;


public class Hitable : MonoBehaviour
{

    public float InitialHits = 100f;

    private float _hits;

    private float _lastHitTime = -10000f;
    private float _hitFlash = 0f;


    public delegate void HitableEvent(Hitable hitable);

    public event HitableEvent OnHit;
    public event HitableEvent OnDead;
    
    public LegOwner Owner;
    private EffectApplier _applier;

    public float Hits { get => _hits; set => _hits = value; }

    public float InvincibleTime = 0f;

    private WeaponSounder _sounder;

    public void Start()
    {
        Revive();
        _applier = EffectApplier.GetInstance(GetComponent<SpriteRenderer>());

        if (_sounder == null) _sounder = GetComponent<WeaponSounder>();
        if (_sounder == null) _sounder = GetComponentInChildren<WeaponSounder>();
        if (_sounder == null) _sounder = GetComponentInParent<WeaponSounder>();
        if (_sounder == null && transform.parent != null) _sounder = transform.parent.GetComponentInChildren<WeaponSounder>();
    }

    public void DoHit(float damage)
    {
        Debug.Log(name + " get damage " + damage + ", hits = " + _hits + ", owner = " + Owner + (InvincibleTime > 0 ? ", btw we invincible" : ""));
        if (_hits <= 0 || Owner == null || InvincibleTime > 0) return;
        _lastHitTime = Time.time;
        _hits -= damage;
        //Owner.OnPartHitCall(this);
        OnHit?.Invoke(this);
        if (_hits <= 0)
        {
            OnDead?.Invoke(this);
            //Owner.OnPartLostCall(this);
        }

        if (_sounder != null) _sounder.PlaySoundHit();
    }

    public void Revive(float withHits = -1f)
    {
        if (withHits <= 0) withHits = InitialHits;
        _hits = withHits;
    }

    public void Update()
    {
        if (InvincibleTime > 0) InvincibleTime -= Time.deltaTime;
        var _oldHit = _hitFlash;
        _hitFlash = Util.SmoothFlash(_oldHit, _lastHitTime, 0.5f);
        if (_oldHit != _hitFlash)
        {
            _applier.Damaging = _hitFlash;
            //Debug.Log(name + ": hit = " + _hitFlash);
        }
    }
}
