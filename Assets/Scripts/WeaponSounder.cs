﻿using UnityEngine;

public class WeaponSounder : MonoBehaviour
{
    public AudioSource sounder;
    public AudioClip[] soundSpark;
    public AudioClip[] soundBigSpark;

    public AudioClip[] soundAttach;
    public AudioClip[] soundDeattach;

    public AudioClip[] soundHit;

    public void PlaySoundBigSpark()
    {
        PlayRandomSound(soundBigSpark);
    }

    public void PlaySoundSpark()
    {
        PlayRandomSound(soundSpark);
    }

    public void PlaySoundAttach()
    {
        PlayRandomSound(soundAttach);
    }

    public void PlaySoundDeattach()
    {
        PlayRandomSound(soundDeattach);
    }

    public void PlaySoundHit()
    {
        PlayRandomSound(soundHit);
    }

    void PlayRandomSound(AudioClip[] list)
    {
        if (list == null || list.Length == 0 || sounder == null) return;
        var sound = Random.Range(0, list.Length);
        sounder.clip = list[sound];
        if (sounder.clip != null) sounder.Play();
    }
}
