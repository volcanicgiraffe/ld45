﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyComeGetSome : StateMachineBehaviour
{
    private Rigidbody2D _rigidbody2D;
    private GameObject _player;
    private EnemyScript _enemy;

    // OnStateEnter is called when a transition starts and the state machine starts to evaluate this state
    override public void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        _player = HeroSoulController.Hero;
        _rigidbody2D = animator.GetComponent<Rigidbody2D>();
        _enemy = animator.GetComponent<EnemyScript>();
    }

    // OnStateUpdate is called on each Update frame between OnStateEnter and OnStateExit callbacks
    override public void OnStateUpdate(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        if (_player == null || _enemy == null) return; //todo: need trigger on that
        var direction = Mathf.Sign(_player.transform.position.x - _rigidbody2D.position.x);
        _rigidbody2D.AddForce(new Vector2(_enemy.ComeSpeed*direction, 0f), ForceMode2D.Impulse);
        _rigidbody2D.transform.localScale = new Vector3(-direction, 1, 1);
        
    }

    // OnStateExit is called when a transition ends and the state machine finishes evaluating this state
    //override public void OnStateExit(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    //{
    //    
    //}

    // OnStateMove is called right after Animator.OnAnimatorMove()
    //override public void OnStateMove(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    //{
    //    // Implement code that processes and affects root motion
    //}

    // OnStateIK is called right after Animator.OnAnimatorIK()
    //override public void OnStateIK(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    //{
    //    // Implement code that sets up animation IK (inverse kinematics)
    //}
}
