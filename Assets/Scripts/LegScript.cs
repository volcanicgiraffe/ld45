﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LegScript : MonoBehaviour
{
    private HingeJoint2D _joint;
    private Hitable _hitable;
    public LegOwner Owner;

    private LegSocket _socket;

    public LegOwner.LegEvent OnLegDetach;

    private LegScript _attachedTo = null;

    private SpriteRenderer _spriteRenderer;

    private WeaponSounder _sounder;

    private EffectApplier _applier;
    private LegOwner.Side _cachedSide;
    
    void Start()
    {
        _joint = GetComponent<HingeJoint2D>();
        _hitable = GetComponent<Hitable>();
        if (!_hitable) _hitable = gameObject.AddComponent<Hitable>();
        _hitable.OnDead += OnDead;
        _spriteRenderer = GetComponent<SpriteRenderer>();
        _sounder = GetComponentInChildren<WeaponSounder>();
        _applier = EffectApplier.GetInstance(_spriteRenderer);
        AttachTo(null, null);
    }

    private void OnDead(Hitable hitable)
    {
        OnDeador.Instance.OnDead1(this.transform);
        OnDetach(hitable);
        Destroy(gameObject);
    }

    void Update()
    {
        _applier.DrawLeg(_cachedSide);
    }

    public void Detach()
    {
        OnDetach(null);
    }
    void OnDetach(Hitable hitable)
    {
        if (_sounder != null) _sounder.PlaySoundDeattach();

        Owner.OnPartLostCall(this);
        AttachTo(null, null);
        _joint.connectedBody = null;
        _joint.enabled = false;
        if (_socket != null) _socket.OnDetach();
        _socket = null;
    }

    private void AttachTo(LegScript leg, LegOwner owner)
    {
        if (_attachedTo != null)
        {
            _attachedTo.OnLegDetach -= OnParentLegDetach;
        }

        if (Owner != null)
        {
            Owner.OnOwnerDead -= OwnerOnOnOwnerDead;
            Owner.OnDisconnect -= OwnerOnOnDisconnect;
        }

        _attachedTo = leg;
        Owner = owner;
        _hitable.Owner = owner;
        if (_attachedTo != null)
        {
            _attachedTo.OnLegDetach += OnParentLegDetach;
        }

        if (Owner != null)
        {
            Owner.OnOwnerDead += OwnerOnOnOwnerDead;
            Owner.OnDisconnect += OwnerOnOnDisconnect;
        }

        if (Owner != null && Owner.side == LegOwner.Side.Player)
        {
            if (_sounder != null) _sounder.PlaySoundAttach();

            GlobalHelp.HideHelpAttach();
        }

        _cachedSide = Owner == null ? LegOwner.Side.None : Owner.side;
    }

    private void OwnerOnOnDisconnect()
    {
        OnDetach(null);
    }

    private void OwnerOnOnOwnerDead()
    {
        OnDetach(null);
    }

    private void OnParentLegDetach(LegScript leg)
    {
        OnDetach(null);
    }


    public void AttachTo(LegSocket socket)
    {
        _socket = socket;
        AttachTo(_socket.GetLeg(), _socket.GetOwner());

        Owner = socket.GetOwner();
        _hitable.Owner = Owner;

        socket.Occupy();
        _joint.connectedBody = socket.GetRig();
        _joint.enabled = true;

        _joint.autoConfigureConnectedAnchor = false;
        _joint.connectedAnchor = socket.transform.localPosition;
    }
}
