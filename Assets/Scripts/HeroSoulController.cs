using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;


public class HeroSoulController : MonoBehaviour
{
    private LegOwner _body;
    private static bool _insideBody;
    private Floaty _defaultsFloaty;
    private FollowMouseForce _defaultsMouseForce;

    private Hitable _bodyHitable;
    public GameObject CorePrefab;

    public static GameObject Hero;

    private HeroScript _bodyHero;
    private Hitable _ownHitable;

    public GameObject OnDeadPrefab;

    private void Start()
    {
        _insideBody = false;
        _defaultsFloaty = transform.GetChild(0).GetComponent<Floaty>();
        _defaultsMouseForce = transform.GetChild(0).GetComponent<FollowMouseForce>();
        Hero = gameObject;
        _ownHitable = GetComponent<Hitable>();
        _ownHitable.OnDead += OnSoulDead;
    }

    public Hitable GetHitable()
    {
        return _insideBody ? _bodyHitable : null;
    }

    public HeroScript GetHeroScript()
    {
        return _insideBody ? _bodyHero : null;
    }
    
    public static bool IsInsideBody
    {
        get { return _insideBody; }
    }

    private void OnSoulDead(Hitable hitable)
    {
        Debug.Log("GAME OVER");
        GlobalSounder.PlaySoundPuff();

        Hero = null;
        var onDead = Instantiate(OnDeadPrefab);
        onDead.transform.position = transform.position;
        OnDeador.Instance.StartCoroutine(OnLoose()); //ugly
        Destroy(gameObject);
    }

    private static IEnumerator OnLoose()
    {
        yield return new WaitForSeconds(2);
        SceneManager.LoadScene("GameOver");
    }


    public void ReleaseFromBody()
    {
        if (!_insideBody) return;
        Debug.Log("Release body");
        
        _body.GetComponent<LegOwner>().OnOwnerDead -= OnBodyDead;

        //removes himself from child, shows spark again
        _body.GetComponent<Floaty>().enabled = false;
        _body.GetComponent<FollowMouseForce>().enabled = false;
        Destroy(_body.GetComponent<HeroScript>());

        _body.ReleaseLegs(); //todo: destroy legs instead
        _insideBody = false;
        _body.side = LegOwner.Side.None;
        transform.SetParent(null);
        gameObject.SetActive(true);
        _body.gameObject.layer = LayerMask.NameToLayer("Default");

        Destroy(_body.gameObject);
        _body = null;
        _bodyHitable = null;
        _bodyHero = null;

        GlobalSounder.PlaySoundUnlock();
        GlobalHelp.HideHelpEject();
    }

    public void ReleaseLegs()
    {

        //just releases all legs
        if (_insideBody)
        {
            Debug.Log("Release legs");
            _body.ReleaseLegs();

            GlobalHelp.HideHelpRelease();
        }
    }

    public void FillBody(LegOwner freeBody)
    {
        if (_insideBody) return;
        Debug.Log("Fill body " + freeBody.name);
        //adds himself as child, hides spark
        var floaty = freeBody.GetComponent<Floaty>();
        if (floaty == null)
        {
            floaty = freeBody.gameObject.AddComponent<Floaty>();
            //todo: ugly
            floaty.FloatForce = _defaultsFloaty.FloatForce;
            floaty.HighDistance = _defaultsFloaty.HighDistance;
            floaty.LowDistance = _defaultsFloaty.LowDistance;
            floaty.GravDampenMax = _defaultsFloaty.GravDampenMax;
            floaty.GravDampenScale = _defaultsFloaty.GravDampenScale;
        }
        var mouseController = freeBody.GetComponent<FollowMouseForce>();
        if (mouseController == null)
        {
            mouseController = freeBody.gameObject.AddComponent<FollowMouseForce>();
            mouseController.DistanceCoefficient = _defaultsMouseForce.DistanceCoefficient;
            mouseController.maxPullForce = _defaultsMouseForce.maxPullForce;
            mouseController.minPullForce = _defaultsMouseForce.minPullForce;
            mouseController.DistanceDeadZone = _defaultsMouseForce.DistanceDeadZone;
        }

        floaty.enabled = true;
        mouseController.enabled = true;
        freeBody.GetComponent<Rigidbody2D>().drag = 10;

        freeBody.gameObject.AddComponent<HeroScript>().soul = this;

        _body = freeBody;
        _insideBody = true;
        _body.side = LegOwner.Side.Player;
        transform.SetParent(_body.transform);
        transform.localPosition = Vector3.zero;
        gameObject.SetActive(false);
        _body.Revive();

        _body.gameObject.layer = LayerMask.NameToLayer("player");

        _bodyHitable = _body.GetComponent<Hitable>();
        _bodyHero = _body.GetComponent<HeroScript>();

        if (!CorePrefab) Debug.LogError("Set up core prefab to soul!");
        else
        {
            var core = Instantiate(CorePrefab);
            core.transform.parent = _body.transform;
            core.transform.localPosition = Vector3.zero;
        }
        
        _body.GetComponent<LegOwner>().OnOwnerDead += OnBodyDead;

        GlobalSounder.PlaySoundLock();
        GlobalHelp.HideHelpPick();
    }

    private void OnBodyDead()
    {
        ReleaseFromBody();
        _ownHitable.InvincibleTime = 2;
    }


    public void OnBodyTrigger(Collider2D other)
    {
        if (_insideBody) return;
        var legOwner = other.GetComponent<LegOwner>();
        if (legOwner != null && legOwner.side == LegOwner.Side.None)
        {
            FillBody(legOwner);
        }
    }
}
