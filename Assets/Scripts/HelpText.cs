﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HelpText : MonoBehaviour
{
    public Transform target;

    private Vector3 _diff = Vector3.zero;

    public Animator anim;

    private bool _hidden = false;

    void Start()
    {
        if (target != null) _diff = transform.position - target.transform.position;

    }

    void Update()
    {
        if (target != null) transform.position = target.transform.position + _diff;
    }

    public void HideHelp()
    {
        if (_hidden) return;

        anim.SetTrigger("HelpHideT");
        _hidden = true;
    }

    public void ShowHelp()
    {
        if (!_hidden) return;
        anim.SetTrigger("HelpShowT");
        _hidden = false;
    }
}
