﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OnDeador : MonoBehaviour
{
    public static OnDeador Instance;

    public GameObject OnDeadPrefab;
    // Start is called before the first frame update
    void Start()
    {
        Instance = this;
    }

    public void OnDead1(Transform transform)
    {
        var ps = Instantiate(OnDeadPrefab, transform.position, Quaternion.identity);
        Destroy(ps, 5.0f);
    }
}
