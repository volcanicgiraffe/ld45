using System;
using UnityEngine;


public class Weapon : MonoBehaviour
{

    public float DamagePerHit = 10f;
    private Hitable _hitable;

    public enum WeaponType
    {
        Default, Core, Blade
    }

    public WeaponType Type = WeaponType.Default;

    private float _reloadTimer = 0;
    private const float ReloadTimeS = 0.5f;

    private WeaponSounder _sounder;

    private void Awake()
    {
        _hitable = GetComponent<Hitable>();
        if (_hitable == null) _hitable = GetComponentInParent<Hitable>();
        if (_hitable == null)
        {
            throw new InvalidOperationException("Hitable component shall be on this object or parent");
        }

        if (_sounder == null) _sounder = GetComponent<WeaponSounder>();
        if (_sounder == null) _sounder = GetComponentInChildren<WeaponSounder>();
        if (_sounder == null) _sounder = GetComponentInParent<WeaponSounder>();
        if (_sounder == null && transform.parent != null) _sounder = transform.parent.GetComponentInChildren<WeaponSounder>(); // for blades
    }

    public void Update()
    {
        if (_reloadTimer > 0)
        {
            _reloadTimer -= Time.deltaTime;
        }
    }

    public void OnHitMade()
    {
        _reloadTimer = ReloadTimeS;
    }

    private bool IsBlade => Type == WeaponType.Blade;

    public float GetDamageTo(Weapon weapon)
    {
        var factor = 1f;
        //blade x blade - factor 0.25
        //anything x blade - 0

        if (this.IsBlade && weapon.IsBlade) factor = 0.25f; //todo: or just smaller?
        if (!this.IsBlade && weapon.IsBlade) factor = 0f;
        return _reloadTimer > 0 ? 0 : DamagePerHit * factor;
    }

    public LegOwner GetOwner()
    {
        if (_hitable == null)
        {
            throw new Exception(name + " does not have hitable!");
        }
        return _hitable.Owner;
    }

    private void OnCollisionEnter2D(Collision2D other)
    {
        if (GetOwner() == null) return;
        var weapon = other.gameObject.GetComponent<Weapon>();
        //Debug.Log("collision " + name + " with " + weapon);
        if (weapon == null || weapon.GetOwner() == null || !CanBeat(weapon.GetOwner().side, GetOwner().side))
        {
            return;
        }

        //todo: check velocity
        float damage = weapon.GetDamageTo(this);
        bool detach = false;
        Debug.Log("Collide magnitude:" + other.relativeVelocity.magnitude.ToString());
        if (this.IsBlade && weapon.IsBlade)
        {
            if (other.relativeVelocity.magnitude > 40)
            {// For future: less than 40 are glance hits, 70 is a really strong hit
                EffectManager.CreateBigSpark(other.GetContact(0).point);
                EffectManager.Slowdown();
                EffectManager.CamShake();
                if (_sounder != null) _sounder.PlaySoundBigSpark();
            }
            else
            {
                EffectManager.CreateSpark(other.GetContact(0).point);
                if (_sounder != null) _sounder.PlaySoundSpark();
            }
            if (other.relativeVelocity.magnitude > 60)
                detach = true;
        }
        if (damage > 0)
        {
            Debug.Log(name + " get hit by " + weapon.name + " = " + damage);
            weapon.OnHitMade();
            _hitable.DoHit(damage);

        }
        if (detach)
        {
            Debug.Log(name + " detached by a strong hit");
            LegScript ls;
            ls = weapon.GetComponent<LegScript>();
            // doesnt work with more child levels
            if(ls == null && weapon.transform.parent != null) ls = weapon.transform.parent.GetComponent<LegScript>();
            if (ls != null) ls.Detach();
        }
    }

    bool CanBeat(LegOwner.Side side1, LegOwner.Side side2)
    {
        return side1 != LegOwner.Side.None && side2 != LegOwner.Side.None && side1 != side2;
    }
}
