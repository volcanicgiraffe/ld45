using System;
using UnityEngine;


public class PhysicRotator : MonoBehaviour
{
    public float PhRotation = 0;
    private Rigidbody2D _rigidbody2D;
    private AudioSource _sounder;

    public AudioClip[] soundWhip;

    private void Start()
    {
        _rigidbody2D = GetComponent<Rigidbody2D>();
        _sounder = GetComponent<AudioSource>();
    }

    private void FixedUpdate()
    {
        _rigidbody2D.MoveRotation(PhRotation);
    }

    public void PlayWhipSound()
    {
        PlayRandomSound(soundWhip);
    }

    void PlayRandomSound(AudioClip[] list)
    {
        if (list == null || list.Length == 0 || _sounder == null) return;
        var sound = UnityEngine.Random.Range(0, list.Length);
        _sounder.clip = list[sound];
        if (_sounder.clip != null) _sounder.Play();
    }
}
