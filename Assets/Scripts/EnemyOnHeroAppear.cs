﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyOnHeroAppear : MonoBehaviour
{
    private void OnTriggerStay2D(Collider2D other)
    {
        GetComponentInParent<Animator>().SetBool("Active", true);
    }

    private void OnTriggerExit2D(Collider2D other)
    {
        GetComponentInParent<Animator>().SetBool("Active", false);
    }

}
