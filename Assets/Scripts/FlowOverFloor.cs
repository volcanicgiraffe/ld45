using System;
using UnityEngine;

[RequireComponent(typeof(Rigidbody2D))]
public class FlowOverFloor : MonoBehaviour
{
    public float FloorDistance = 3f;
    public float GravityScale = 1f;
        
    public bool PrintDebug = false;
    private float _cv;
    
    private Rigidbody2D _rigidbody2D;

    private void Start()
    {
        _rigidbody2D = GetComponent<Rigidbody2D>();
    }

    private void FixedUpdate()
    {
        
        RaycastHit2D hit = Physics2D.Raycast(transform.position, -Vector2.up, FloorDistance*2f, LayerMask.GetMask("terrain"));
        if (hit.collider != null)
        {
            var dy = FloorDistance - hit.distance;
            _rigidbody2D.gravityScale = -(dy/FloorDistance) * GravityScale;
            //dy = Mathf.SmoothDamp(0, dy, ref _cv, 0.1f);
            if (PrintDebug) Debug.Log("y = " + _rigidbody2D.position.y + ", distance to floor = " + hit.distance + ", dy = " + dy);
            if (dy > 0)
            {
                //_rigidbody2D.AddForce(transform.up * _rigidbody2D.mass/(hit.distance/2));
            }
            //else -> gravity

            //_rigidbody2D.MovePosition(new Vector2(_rigidbody2D.position.x, _rigidbody2D.position.y + dy));
        }
        else
        {
            _rigidbody2D.gravityScale = 1*GravityScale;
        }

    }
}
