﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyRandomizer : MonoBehaviour
{
    public GameObject[] enemyPrefabs;

    public GameObject[] connectorPrefabs;
    public GameObject[] weaponPrefabs;

    public int connectorsMax = 5;

    public Color GizmosColor = new Color(0.5f, 0.5f, 0.5f, 0.4f);

    private float socketChance = 0.6f;
    private float weaponChance = 0.8f;

    public bool SpawnOnStart = true;

    void Start()
    {
        if(SpawnOnStart) SpawnEnemy();
    }

    public GameObject SpawnEnemy()
    {
        var enemy = enemyPrefabs[Random.Range(0, enemyPrefabs.Length)];
        var result = Instantiate(enemy, transform);

        LegSocket[] sockets = enemy.GetComponentsInChildren<LegSocket>();

        if (sockets.Length == 0) return result;

        bool atLeastOne = false;

        for (int i = 0; i < sockets.Length; i++)
        {
            if (Random.Range(0f, 1f) <= socketChance)
            {
                PopulateSocket(sockets[i]);
                atLeastOne = true;
            }
        }

        if (!atLeastOne)
        {
            PopulateSocket(sockets[Random.Range(0, sockets.Length)]);
        }
        return result;
    }

    void PopulateSocket(LegSocket socket)
    {
        var connectors = Random.Range(1, connectorsMax);

        Vector3 prevPosition = socket.transform.parent.transform.position;
        Vector3 position = socket.transform.position;

        for (int i = 0; i < connectors; i++)
        {

            var prefab = connectorPrefabs[Random.Range(0, connectorPrefabs.Length)];

            Vector3 newPosition = SpawnItem(prevPosition, position, prefab);

            prevPosition = position;
            position = newPosition;
        }

        if (Random.Range(0f, 1f) <= weaponChance)
        {
            SpawnItem(prevPosition, position, weaponPrefabs[Random.Range(0, weaponPrefabs.Length)]);
        }
    }

    Vector3 SpawnItem(Vector3 prevPosition, Vector3 position, GameObject prefab)
    {
        Vector3 newPosition = position + (position - prevPosition);

        var angle = Vector3.Angle(prevPosition - position, Vector3.right);

        if (position.y > prevPosition.y)
        {
            angle -= 90;

            if (position.x < prevPosition.x) angle += 180;
        }
        else
        {
            angle -= 180;
        }
        Instantiate(prefab, transform.position + newPosition, Quaternion.Euler(new Vector3(0, 0, angle)));

        return newPosition;
    }

    void OnDrawGizmos()
    {
        Gizmos.color = GizmosColor;
        Gizmos.DrawCube(transform.position, transform.localScale);
    }

}
