﻿using UnityEngine;

[RequireComponent(typeof(Rigidbody2D))]
public class Floaty : MonoBehaviour
{
    private Rigidbody2D _rigidbody2D;
    private float _originalGrav;
    // float when
    public float LowDistance = 5;
    // fall after
    public float HighDistance = 30;
    // floaty strength
    public float FloatForce = 0.6f;
    // how fast will start to fall
    public float GravDampenScale = 10;
    // maximum fall force
    public float GravDampenMax = 40;

    // Start is called before the first frame update
    void Start()
    {
        _rigidbody2D = GetComponent<Rigidbody2D>();
        _originalGrav = _rigidbody2D.gravityScale;
    }

    // Update is called once per frame
    void Update()
    {
        Debug.DrawRay(transform.position, Vector2.down * HighDistance, Color.red);
        Debug.DrawRay(transform.position, Vector2.down* LowDistance, Color.yellow);

        // Push from the terrain from certain height
        if (Physics2D.Raycast(transform.position, Vector2.down, LowDistance, LayerMask.GetMask("terrain")))
            _rigidbody2D.AddForce(new Vector2(Random.Range(-0.1f, 0.1f), 1).normalized * _rigidbody2D.mass* FloatForce, ForceMode2D.Impulse);
        
        // Start to slowly fall down once too high
        if (!Physics2D.Raycast(transform.position, Vector2.down, HighDistance, LayerMask.GetMask("terrain")))
            _rigidbody2D.gravityScale= Mathf.Min(_rigidbody2D.gravityScale+Time.deltaTime* GravDampenScale, GravDampenMax);
        else
            _rigidbody2D.gravityScale = _originalGrav;
    }
}
