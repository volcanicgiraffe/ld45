﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MusicSwitcher : MonoBehaviour
{
    public AudioSource[] _audios;
    private float _remainingFade = 0;
    private const float FadeTime = 4;

    private bool _secondPlaying = false;

    public float TargetVolume = 1;

    // Start is called before the first frame update
    void Start()
    {
        this._audios[0].volume = TargetVolume;
    }

    // Update is called once per frame
    void Update()
    {
        if (_remainingFade > 0)
        {
            this._audios[0].volume -= TargetVolume*Time.deltaTime / FadeTime;
            this._audios[1].volume += TargetVolume*Time.deltaTime / FadeTime;
            _remainingFade -= Time.deltaTime;
        }
        else if (_secondPlaying)
        {
            if (!_audios[1].isPlaying) // for webgl
            {
                _audios[1].loop = true;
                _audios[1].Play();
            }
        }
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        GetComponent<BoxCollider2D>().enabled = false;
        _audios[0].loop = false;
        _audios[1].loop = true;
        _remainingFade = FadeTime;
        _secondPlaying = true;

        //16/8 = 2
        _audios[1].PlayDelayed((2 - Mathf.Repeat(_audios[0].time, 2)));
    }
    
   
}
