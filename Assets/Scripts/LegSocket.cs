﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LegSocket : MonoBehaviour
{
    private bool occupied = false;

    private LegScript attachedLeg; // only one leg
    private LegOwner attachedOwner; // OR only one owner

    private void Start()
    {
        attachedLeg = GetComponentInParent<LegScript>();
        attachedOwner = GetComponentInParent<LegOwner>();
    }

    public bool CanOccupy()
    {
        return !occupied;
    }

    public void Occupy()
    {
        occupied = true;
    }

    public void OnDetach()
    {
        occupied = false;
    }

    public LegScript GetLeg()
    {
        return attachedLeg;
    }

    public LegOwner GetOwner()
    {
        if (attachedLeg != null) return attachedLeg.Owner;
        if (attachedOwner != null) return attachedOwner;

        throw new System.Exception("Socket must be attached to leg or legowner!");
    }

    public Rigidbody2D GetRig()
    {
        if (attachedLeg != null) return attachedLeg.GetComponent<Rigidbody2D>();
        if (attachedOwner != null) return attachedOwner.GetComponent<Rigidbody2D>();

        throw new System.Exception("Socket must be attached to leg or legowner!");
    }

    private bool MyOwnerWantsToGrab()
    {
        if (attachedOwner == null && (attachedLeg == null || attachedLeg.Owner == null)) return false;

        return GetOwner().WantToGrab();
    }

    private void OnTriggerStay2D(Collider2D collision)
    {
        if (CanOccupy() && collision.tag == "magnet" && MyOwnerWantsToGrab())
        {
            var leg = collision.GetComponentInParent<LegScript>();

            if (leg != null && leg.Owner == null)
            {
                leg.AttachTo(this);
            }
        }
    }
}
