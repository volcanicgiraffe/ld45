﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class GlobalHelp
{
    public static GlobalHelpMB ghmb;

    private static bool donePick = false;
    private static bool doneAttach = false;
    private static bool doneSwing = false;
    private static bool doneEject = false;
    private static bool doneRelease = false;

    public static void Reset()
    {
        donePick = false;
        doneAttach = false;
        doneSwing = false;
        doneEject = false;
        doneRelease = false;
    }


    public static void HideHelpMove()
    {
        ghmb.HideHelp(ghmb.helpMove);
    }

    public static void HideHelpPick()
    {
        if (donePick) return;
        donePick = true;

        ghmb.HideHelp(ghmb.helpPick);
        if (!doneAttach) ghmb.ShowHelp(ghmb.helpAttach);
    }
    public static void HideHelpAttach()
    {
        if (doneAttach) return;
        doneAttach = true;

        ghmb.HideHelp(ghmb.helpAttach);
        if (!doneSwing) ghmb.ShowHelp(ghmb.helpSwing);
    }
    public static void HideHelpSwing()
    {
        if (doneSwing) return;
        doneSwing = true;

        ghmb.HideHelp(ghmb.helpSwing);
        if (!doneRelease) ghmb.ShowHelp(ghmb.helpRelease);
    }
    public static void HideHelpEject()
    {
        if (doneEject) return;
        doneEject = true;
        ghmb.HideHelp(ghmb.helpEject);
    }
    public static void HideHelpRelease()
    {
        if (doneRelease) return;
        doneRelease = true;
        ghmb.HideHelp(ghmb.helpRelease);
    }

}
public class GlobalHelpMB : MonoBehaviour
{
    public HelpText helpMove;
    public HelpText helpPick;
    public HelpText helpAttach;
    public HelpText helpSwing;
    public HelpText helpEject;
    public HelpText helpRelease;

    private float moveTimer = 5f;

    private void Start()
    {
        GlobalHelp.ghmb = this;
        GlobalHelp.Reset();

        HideHelp(helpAttach);
        HideHelp(helpSwing);
        HideHelp(helpRelease);
    }

    public void HideHelp(HelpText help)
    {
        if (help != null) help.HideHelp();
    }

    public void ShowHelp(HelpText help)
    {
        if (help != null) help.ShowHelp();
    }



    private void Update()
    {
        if (moveTimer >= 0)
        {
            moveTimer -= Time.deltaTime;
            if (moveTimer < 0) HideHelp(helpMove);
        }
    }
}
