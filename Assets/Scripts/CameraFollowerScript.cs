﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraFollowerScript : MonoBehaviour
{
    public GameObject Player;
    private Vector3 velocity = Vector3.zero;

    public GameObject LeftLimit;
    public GameObject RightLimit;
    private Camera _camera;
    private float _camHeight;
    private float _camWidth;

    private const float Edge = 0.3f;
    // Start is called before the first frame update
    void Start()
    {
        _camera = GetComponent<Camera>();
        _camHeight = 2f * _camera.orthographicSize;
        _camWidth = _camHeight * _camera.aspect;
    }

    // Update is called once per frame
    void Update()
    {
        Vector3 point = _camera.WorldToViewportPoint(Player.transform.position);
        Vector3 delta = Player.transform.position - _camera.ViewportToWorldPoint(new Vector3(0.5f, 0.5f, point.z)); //(new Vector3(0.5, 0.5, point.z));
        Vector3 destination = new Vector3(
            Mathf.Clamp(transform.position.x + delta.x, LeftLimit.transform.position.x+_camWidth/2, RightLimit.transform.position.x-_camWidth/2),
        transform.position.y, transform.position.z);
        if (point.x > (1-Edge) || point.x < Edge)
        {
            transform.position = Vector3.SmoothDamp(transform.position, destination, ref velocity, 0.2f);
        }
    }
}
