using System;
using UnityEngine;


public class EnemySoulOut : MonoBehaviour
{

    private EffectApplier _applier;
    private float _startTime;

    private void Start()
    {
        _applier = GetComponent<EffectApplier>();
        _startTime = Time.time;
        Destroy(this, 4);
    }

    private void Update()
    {
        _applier.InkFilling = Util.Interpolate(_applier.InkFilling, 3, 0);
        _applier.Glowing = Util.SmoothFlash(_applier.Glowing, _startTime, 2);
    }
}
