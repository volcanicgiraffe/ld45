﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyAttackHoming : StateMachineBehaviour
{
    private Rigidbody2D _rigidbody2D;
    private GameObject _player;
    public float minPullForce = 0f;
    public float maxPullForce = 500f;
    public float DistanceCoefficient = 100f;
    override public void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        _player = HeroSoulController.Hero;
        _rigidbody2D = animator.GetComponent<Rigidbody2D>();
    }

    // OnStateUpdate is called on each Update frame between OnStateEnter and OnStateExit callbacks
    override public void OnStateUpdate(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        if (_player == null) return;
        var direction = _player.transform.position - animator.transform.position;
        var coeff = Mathf.Clamp(direction.magnitude * DistanceCoefficient, minPullForce, maxPullForce);
        direction.Normalize();
        _rigidbody2D.AddForce(new Vector2(direction.x, direction.y) * coeff, ForceMode2D.Impulse);
    }

    // OnStateExit is called when a transition ends and the state machine finishes evaluating this state
    //override public void OnStateExit(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    //{
    //    
    //}

    // OnStateMove is called right after Animator.OnAnimatorMove()
    //override public void OnStateMove(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    //{
    //    // Implement code that processes and affects root motion
    //}

    // OnStateIK is called right after Animator.OnAnimatorIK()
    //override public void OnStateIK(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    //{
    //    // Implement code that sets up animation IK (inverse kinematics)
    //}
}
