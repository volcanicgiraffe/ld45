﻿using UnityEngine;
using UnityEngine.Serialization;

public class FollowMouseForce : MonoBehaviour
{
    private Rigidbody2D _rigidbody2D;
    public float minPullForce = 0f;
    public float maxPullForce = 2f;
    public float DistanceCoefficient = 0.5f;
    public float DistanceDeadZone = 0.1f;

    // Start is called before the first frame update
    void Start()
    {
        _rigidbody2D = GetComponent<Rigidbody2D>();
    }

    // Update is called once per frame
    void Update()
    {
        var mouse = Input.mousePosition;
        var position = Camera.main.ScreenToWorldPoint(mouse);
        var direction = position - transform.position;
        if (direction.magnitude < DistanceDeadZone) return;
        var coeff = Mathf.Clamp(direction.magnitude * DistanceCoefficient, minPullForce, maxPullForce);
        direction.Normalize();
        
        _rigidbody2D.AddForce(new Vector2(direction.x, direction.y)* coeff, ForceMode2D.Impulse);
    }
}
