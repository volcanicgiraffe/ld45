﻿using UnityEngine;

public class HeroScript : MonoBehaviour
{
    private LegOwner _legOwner;
    private Hitable _hitable;
    public float RotEnergy = 100;
    public float RotRestoreCoefficient = 20;
    public float RotSuckCoefficient = 30;
    private Rigidbody2D _rigidbody2D;
    internal HeroSoulController soul;
    
    // Start is called before the first frame update
    void Start()
    {
        _legOwner = GetComponent<LegOwner>();
        _legOwner.side = LegOwner.Side.Player;
        _rigidbody2D = GetComponent<Rigidbody2D>();
        _hitable = GetComponent<Hitable>();
        _hitable.OnDead += OnCoreDead;
    }

    private void OnCoreDead(Hitable hitable)
    {
        Debug.Log("DEAD HERO");
        _legOwner.OnOwnerDeadCall();
        Destroy(gameObject);
    }

    // Update is called once per frame
    void Update()
    {
        RotEnergy = Mathf.Min(RotEnergy + Time.deltaTime*RotRestoreCoefficient, 100);
        var rt = Input.GetAxis("Mouse ScrollWheel");
        if (rt != 0 && RotEnergy > 0)
        {
            RotEnergy -= Mathf.Abs(rt* RotSuckCoefficient);
            _rigidbody2D.AddTorque(rt*560*(_rigidbody2D.mass/120), ForceMode2D.Impulse);
        }

        if (Input.GetButtonDown("Connect"))
        {
            _legOwner.SetWantToGrab(true);
        }
        if (Input.GetButtonUp("Connect"))
        {
            _legOwner.SetWantToGrab(false);
        }
        
        if (Input.GetButtonDown("Eject"))
        {
            soul.ReleaseFromBody();
        }
        else if (Input.GetButtonDown("Disconnect"))
        {
            soul.ReleaseLegs();
        }
    }
}