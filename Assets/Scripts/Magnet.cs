﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Magnet : MonoBehaviour
{
    public GameObject highlighter;

    private void Start()
    {
        HideMagnet(); // it's shown in editor for dev reason
    }

    private void Update()
    {
        if (Input.GetButtonDown("Connect"))
        {
            ShowMagnet();
        }
        if (Input.GetButtonUp("Connect"))
        {
            HideMagnet();
        }
    }

    public void ShowMagnet()
    {
        highlighter.SetActive(true);
    }
    public void HideMagnet()
    {
        highlighter.SetActive(false);
    }

    public bool AllowsToGrab(Collider2D collision)
    {
        return collision.IsTouching(GetComponent<Collider2D>());
    }

}