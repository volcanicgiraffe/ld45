﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using Object = UnityEngine.Object;

public class EnemyScript : MonoBehaviour
{
    private Animator _animator;

    public float ComeSpeed = 50f;
    public float RetreatSpeed = 50f;

    private float _wantToGrabTimer = 3f;

    private LegOwner _legOwner;

    public event Action OnRelease;
    private Hitable _hitable;
    public bool isDead;

    public UnityEvent OnDie;
    // Start is called before the first frame update
    void Start()
    {
        _legOwner = GetComponent<LegOwner>();
        _legOwner.OnLegLost += LegOwnerOnOnLegLost;

        _animator = GetComponent<Animator>();

        _legOwner.SetWantToGrab(true);

        _hitable = GetComponent<Hitable>();
        _hitable.OnDead += OnCoreDead;
        
        //todo: ugly
        DestroyOnRelease(GetComponent<FlowOverFloor>());
        DestroyOnRelease(GetComponent<Floaty>());
        DestroyOnRelease(GetComponent<PhysicRotator>());
    }

    void DestroyOnRelease(Object component)
    {
        if (component != null)
        {
            OnRelease += () => Destroy(component);
        }
    }

    private void OnCoreDead(Hitable hitable)
    {
        isDead = true;
        _legOwner.OnOwnerDeadCall();
        Debug.Log(name + " IS DEAD");
        OnDie.Invoke();
        GlobalHelp.HideHelpSwing();
        Release();
        if (UnityEngine.Random.value > 0.3f) Destroy(gameObject);
    }

    private void LegOwnerOnOnLegLost(LegScript leg)
    {
        _animator.SetTrigger("Just Attacked");
    }

    // Update is called once per frame
    void Update()
    {
        // spasibo Tolik chto zakomentil ne skazav

        if (_wantToGrabTimer > 0)
        {
            _wantToGrabTimer -= Time.deltaTime;
            if (_wantToGrabTimer < 0) _legOwner.SetWantToGrab(false);
        }
    }

    public void Release()
    {
        if (_hitable != null) _hitable.OnDead -= OnCoreDead;
        if (_legOwner != null) _legOwner.OnLegLost -= LegOwnerOnOnLegLost;
        Debug.Log(name + " released from enemy");
        OnRelease?.Invoke();
        if (_animator != null) _animator.enabled = false;
        if (_legOwner != null) _legOwner.side = LegOwner.Side.None;
        Destroy(this);
    }
}
