﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Wave
{
    public Wave(bool isBoss, int maxConnectors, int enemyAmount)
    {
        IsBoss = isBoss;
        MaxConnectors = maxConnectors;
        EnemyAmount = enemyAmount;
    }
    public bool IsBoss { get; set; }
    public bool IsLastBoss { get; set; }
    public int MaxConnectors { get; set; }
    public int EnemyAmount { get; set; }
}

public class ArenaController : MonoBehaviour
{
    bool spawningWave;
    public GameObject LastBoss;
    public GameObject LBSpawnPoint;

    List<Wave> waves = new List<Wave>
    {
        new Wave(false, 1, 1),
        new Wave(false, 1, 2),
        new Wave(false, 2, 2),
        new Wave(false, 3, 2),
        new Wave(true, 1, 1), // first boss
        new Wave(false, 1, 3),
        new Wave(false, 2, 3),
        new Wave(false, 3, 3),
        new Wave(true, 3, 1), // second boss
        new Wave(false, 2, 4),
        new Wave(true, 1, 2), // two first bosses
        new Wave(false, 3, 4),
        new Wave(false, 0,0) {IsLastBoss = true}
    };

    List<EnemyRandomizer> _spawners = new List<EnemyRandomizer>();
    List<GameObject> _activeEnemies = new List<GameObject>();
    private EnemyRandomizer _bossSpawner;

    // Start is called before the first frame update
    void Start()
    {
        var rnds = GameObject.FindGameObjectsWithTag("Randomizer");
        foreach (var rnd in rnds) _spawners.Add(rnd.GetComponent<EnemyRandomizer>());
        _bossSpawner = GameObject.FindGameObjectsWithTag("BossRandomizer")[0].GetComponent<EnemyRandomizer>();
    }

    // TODO CLEANUP GARBAGE AFTER SOME TIME
    void Update()
    {
        var toremove = new List<GameObject>();
        foreach (var enemy in _activeEnemies)
        {
            if (enemy == null || (enemy.GetComponent<EnemyScript>() == null && enemy.GetComponentInChildren<EnemyScript>() == null))
            {
                toremove.Add(enemy);
            }
        }

        foreach (var dead in toremove)
        {
            _activeEnemies.Remove(dead);
        }

        if (!spawningWave && _activeEnemies.Count == 0)
        {
            if (waves.Count == 0)
            {
                SceneManager.LoadScene("WinScreen");
            }
            else SpawnNextWave();
        }
    }

    private void SpawnNextWave()
    {
        spawningWave = true;
        Debug.Log("Arena spawning wave, left " + waves.Count);
        StartCoroutine(WaveSpawner());
    }

    private IEnumerator WaveSpawner()
    {
        if (waves[0].IsLastBoss)
        {
            var e = Instantiate(LastBoss, LBSpawnPoint.transform.position, Quaternion.identity);
            _activeEnemies.Add(e);
        }
        else if (waves[0].IsBoss)
        {
            _bossSpawner.connectorsMax = waves[0].MaxConnectors;
            for (int i = 0; i < waves[0].EnemyAmount; i++)
            {
                var e = _bossSpawner.SpawnEnemy();
                _activeEnemies.Add(e);
                yield return new WaitForSeconds(3);
            }
        }
        else
        {
            foreach (var sp in _spawners)
            {
                sp.connectorsMax = waves[0].MaxConnectors;
            }
            for (int i = 0; i < waves[0].EnemyAmount; i++)
            {
                var e = _spawners[UnityEngine.Random.Range(0, _spawners.Count - 1)].SpawnEnemy();
                _activeEnemies.Add(e);
                yield return new WaitForSeconds(1);
            }
        }
        waves.RemoveAt(0);
        spawningWave = false;
    }
}
