using System;
using UnityEngine;
using UnityEngine.SceneManagement;


public class ExitOnEsc : MonoBehaviour
    {
        private void Update()
        {
            if (Input.GetButtonDown("Cancel")) SceneManager.LoadScene("Intro");
        }
    }
