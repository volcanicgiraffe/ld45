﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CoreController : MonoBehaviour
{
    private Rigidbody2D _rigidbody2D;

    public Rigidbody2D Core;
    
    // Start is called before the first frame update
    void Start()
    {
        _rigidbody2D = GetComponent<Rigidbody2D>();
    }

    // Update is called once per frame
    void Update()
    {
        var mouse = Input.mousePosition;

        var position = Camera.main.ScreenToWorldPoint(mouse);

        _rigidbody2D.MovePosition(position);

        var rt = Input.GetAxis("Mouse ScrollWheel");
        if (rt != 0)
        {
            // Core.freezeRotation = false;
            Core.angularVelocity += rt * 1000;

            // Core.MoveRotation(Core.rotation + rt * 45);
        }
        else
        {
            //Core.freezeRotation = true;
        }

        //Core.transform.Rotate(0, 0, rt*360);
    }
}
