﻿using System;
using UnityEngine;

namespace AlexTrash
{
    public class LegBehaviour : MonoBehaviour
    {
        public GameObject Owner;
        private SpriteRenderer _renderer;
        private float Hits = 100f;
        private Color _oldColor;
        private float _flashTime;
        private Rigidbody2D _rigidbody2D;
        
        // Start is called before the first frame update
        void Start()
        {
            if (Owner == null)
            {
                Owner = transform.parent.gameObject;
            }

            _renderer = GetComponent<SpriteRenderer>();
            _rigidbody2D = GetComponent<Rigidbody2D>();
        }

        // Update is called once per frame
        void Update()
        {
            if (_flashTime > 0)
            {
                _flashTime -= Time.deltaTime;
                if (_flashTime <= 0)
                {
                    _renderer.color = _oldColor;
                }
            }
        }

        void DoDetach()
        {
            var joint2D = GetComponent<HingeJoint2D>();
            joint2D.connectedBody = null;
            joint2D.enabled = false;
            transform.SetParent(null);
        }

        private void OnCollisionEnter2D(Collision2D collision)
        {
            //if (!Owner.CompareTag("Player")) return; //only player processes collisions
            var oppositeLeg = collision.gameObject.GetComponent<LegBehaviour>();
            if (oppositeLeg == null || oppositeLeg.Owner == Owner) return;
            if (collision.gameObject == Owner) return;
            if (collision.rigidbody == null) return;

            //collision.relativeVelocity.magnitude from 4 (just touch) till 90 (smash)
            Debug.Log(name + ": " + _rigidbody2D.velocity.magnitude + ", " + collision.gameObject.name + ": " + collision.rigidbody.velocity.magnitude);
            Debug.Log("Hit " + name + " by " + collision.gameObject.name + " with " + collision.rigidbody.velocity.magnitude);

            var damage = collision.rigidbody.velocity.magnitude;
            if (damage >= 4)
            {
            
                Hits -= damage;
                var animator = GetComponentInParent<Animator>();
                if (animator != null)
                {
                    animator.SetTrigger("Just Attacked");
                }
                if (Hits <= 0)
                {
                    DoDetach();
                }
                else
                {
                    DoFlash();
                }
                
            }

        }

        private void DoFlash()
        {
            if (_flashTime <= 0)
            {
                _oldColor = _renderer.color;
                _renderer.color = Color.yellow;
            }

            _flashTime += 0.5f;
        }
    }
}
