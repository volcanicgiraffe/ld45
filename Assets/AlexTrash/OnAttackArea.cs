﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OnAttackArea : MonoBehaviour
{
    private Animator _animator;

    // Start is called before the first frame update
    void Start()
    {
        _animator = GetComponentInParent<Animator>();
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.CompareTag("Player"))
        {
            _animator.SetTrigger("On Player In Attack Area");
            _animator.ResetTrigger("On Player Out Attack Area");
        }
    }

    private void OnTriggerExit2D(Collider2D other)
    {
        if (other.CompareTag("Player"))
        {
            _animator.SetTrigger("On Player Out Attack Area");
            _animator.ResetTrigger("On Player In Attack Area");
        }
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
