﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[ExecuteAlways]
public class Enemy1 : MonoBehaviour
{
    public float PhRotation = 0;
    private Rigidbody2D _rigidbody2D;

    // Start is called before the first frame update
    void Start()
    {
        _rigidbody2D = GetComponent<Rigidbody2D>();
        #if !UNITY_EDITOR
        transform.localRotation = Quaternion.identity;
        #endif
    }

    // Update is called once per frame
    void Update()
    {
#if UNITY_EDITOR
        transform.localRotation = Quaternion.Euler(0f, 0f, PhRotation);
#else
        _rigidbody2D.MoveRotation(PhRotation);
#endif
    }
    
}
