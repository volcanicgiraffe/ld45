﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HPUIController : MonoBehaviour
{
    public GameObject Hero;
    private HeroSoulController heros;
    private float initialHeight;
    private Image hpBar;
    private Image rotBar;

    // Start is called before the first frame update
    void Start()
    {
        heros = Hero.GetComponent<HeroSoulController>();
        hpBar = transform.GetChild(2).GetComponent<Image>();
        rotBar = transform.GetChild(3).GetComponent<Image>();
        initialHeight = hpBar.rectTransform.sizeDelta.y;
    }

    // Update is called once per frame
    void Update()
    {
        var hp = 0f;
        if (heros)
        {
            var herosHit = heros.GetHitable();
            if (herosHit != null)
                hp = herosHit.Hits / herosHit.InitialHits;
        }
        hpBar.rectTransform.sizeDelta = new Vector2(hpBar.rectTransform.sizeDelta.x, initialHeight * hp);
        var re = heros.GetHeroScript()?.RotEnergy ?? 0;
        rotBar.fillAmount = 0.6f * (re / 100);
    }
}
