Shader "Custom/Paper"
{
	Properties
	{
		[PerRendererData] _MainTex ("Sprite Texture", 2D) = "white" {}
		[PerRendererData] _MaskTex ("Sprite Mask", 2D) = "red" {}
		_Color ("Tint", Color) = (1,1,1,1)
		[MaterialToggle] PixelSnap ("Pixel snap", Float) = 0
		
		[PerRendererData] 
		_DamageFactor ("Damage Factor", Range(0,1)) = 0
		_DamageColor ("Damage Tint", Color) = (1, 0, 0, 1)
		
		[HDR] _InkColor ("Ink Tint", Color) = (0,0,0,1)

        [PerRendererData]
        _Ink ("Ink", Range(0,1)) = 0
        [PerRendererData]
		_Glow ("Glow", Range(0,1)) = 0
		_BlinkSize ("BlinkSize", Range(0,1)) = 0.1
				
		_BlinkColor ("Blink Color", Color) = (1,1,1,1)

        [PerRendererData]
		_BlinkStartedAt("Blink Started", Float) = -1
		_BlinkPeriod("Blink Period", Float) = 2
		_BlinkDuration("Blink Duration", Float) = 1

	}

	SubShader
	{
		Tags
		{ 
			"Queue"="Transparent" 
			"IgnoreProjector"="True" 
			"RenderType"="Transparent" 
			"PreviewType"="Plane"
			"CanUseSpriteAtlas"="True"
		}

		Cull Off
		Lighting Off
		ZWrite Off
		Blend One OneMinusSrcAlpha

		Pass
		{
		CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag
			#pragma multi_compile _ PIXELSNAP_ON
			#include "UnityCG.cginc"
			#include "Noise.hlsl"
			
			struct appdata_t
			{
				float4 vertex   : POSITION;
				float4 color    : COLOR;
				float2 texcoord : TEXCOORD0;
				float2 texcoord1 : TEXCOORD1;
			};

			struct v2f
			{
				float4 vertex   : SV_POSITION;
				fixed4 color    : COLOR;
				float2 texcoord  : TEXCOORD0;
				float2 texcoord1  : TEXCOORD1;
			};
			
			fixed4 _Color;
			fixed4 _DamageColor;
			sampler2D _MaskTex;

			v2f vert(appdata_t IN)
			{
				v2f OUT;
				OUT.vertex = UnityObjectToClipPos(IN.vertex);
				OUT.texcoord = IN.texcoord;
				OUT.texcoord1 = IN.texcoord1;
				OUT.color = IN.color * _Color;
				#ifdef PIXELSNAP_ON
				OUT.vertex = UnityPixelSnap (OUT.vertex);
				#endif

				return OUT;
			}

			sampler2D _MainTex;
			sampler2D _AlphaTex;
			float4 _InkColor;
			fixed4 _BlinkColor;
			float _AlphaSplitEnabled;
            float _DamageFactor;

            float _Ink;
            float _Glow;
            //float _Blink;
            float _BlinkSize;
            
            float _BlinkStartedAt;
            float _BlinkPeriod;
            float _BlinkDuration;
            
            //float4 Time;
            
			fixed4 SampleSpriteTexture (float2 uv)
			{
				fixed4 color = tex2D (_MainTex, uv);

#if UNITY_TEXTURE_ALPHASPLIT_ALLOWED
				//if (_AlphaSplitEnabled)
				//	color.a = tex2D (_AlphaTex, uv).r;
#endif //UNITY_TEXTURE_ALPHASPLIT_ALLOWED

				return color;
			}
			
//float step(float a, float x) {return x >= a;}
// step(a,x) = x >= a ? 1 : 0
			float4 frag(v2f IN) : SV_Target
			{
			
			    float2 offset = 0;//_DamageFactor*pnoise(IN.texcoord.y, IN.texcoord.x)/8.0f;
				float4 c = SampleSpriteTexture (IN.texcoord+offset) * IN.color;				
				float4 m = tex2D(_MaskTex, IN.texcoord1+offset);
				
				//every period perform blink. blink
				float relBlikTime = mod((_Time.y - _BlinkStartedAt), _BlinkPeriod);
				//relBlikTime - from - till Period. need to get 0 for 0, 1 for _BlinkDuration, then 0 again
				float _Blink = step(1, _BlinkStartedAt) * step(_BlinkStartedAt, _Time.y) * relBlikTime * step(relBlikTime, _BlinkDuration)/_BlinkDuration;
				
				float IsOnInkWay = (1-step(_Ink, m.r*m.a))*(1-step(m.r*m.a, 0));
				float IsGlowingPixel = (step(1-_Glow, m.g*m.a))*m.g*m.a;
				float IsBlinking = (step(_Blink-_BlinkSize, m.r*m.a))*(1-step(m.r*m.a, 0))*(step(m.r*m.a, _Blink));
				float IsBlinkingGlow = 0;//(step(1-pow(_Blink,0.5), m.g*m.a))*m.g*m.a/2;
				
				float DamagingBlinking = 0.1 + 0.9*(step(_DamageFactor-_BlinkSize*2, m.r*m.a))*(1-step(m.r*m.a, 0))*(step(m.r*m.a, _DamageFactor));
				
				c.rgb = lerp(
				    lerp(
                        lerp(
                            c.rgb, 
                            lerp(c.rgb, _InkColor.rgb, _InkColor.a),
                            max(IsOnInkWay, IsGlowingPixel)*(1-_DamageFactor*DamagingBlinking)
                        ),
                        _BlinkColor,
                        max(IsBlinking, IsBlinkingGlow)*m.a
                    ),
                    _DamageColor,
                    //(1-step(m.r*m.a, 0))*_DamageFactor
                    _DamageFactor*DamagingBlinking
				);
				
				/*
				c.r = lerp(float3(0,0,0), float3(1,1,1), IsOnInkWay);
				c.b = lerp(float3(0,0,0), float3(1,1,1), IsGlowingPixel);
				c.g = lerp(0, 1, IsOnSpark);*/

				c.rgb *= c.a;
				return c;
			}
		ENDCG
		}
	}
}