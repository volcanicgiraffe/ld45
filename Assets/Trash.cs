﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Trash : MonoBehaviour
{
    public GameObject[] Prefabs;


    public float DropEvery = 4;

    private float _nextDrop = 4;

    public int DropOnStart = 0;
    // Start is called before the first frame update
    void Start()
    {
        for (int i = 0; i < DropOnStart; i++)
        {
            DoDrop();
        }
    }

    // Update is called once per frame
    void Update()
    {
        _nextDrop -= Time.deltaTime;
        if (_nextDrop <= 0)
        {
            _nextDrop = DropEvery;
            DoDrop();
        }
    }

    private void DoDrop()
    {
        var inst = Instantiate(Prefabs[Random.Range(0, Prefabs.Length)], transform);
        inst.GetComponent<Rigidbody2D>().AddTorque(Random.Range(-3, 3), ForceMode2D.Impulse);
    }
}
